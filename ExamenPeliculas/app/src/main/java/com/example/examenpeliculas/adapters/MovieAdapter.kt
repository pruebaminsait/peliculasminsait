package com.example.examenpeliculas.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.examenpeliculas.R
import com.example.examenpeliculas.models.response.Movie
import kotlinx.android.synthetic.main.peliculas_item.view.description_text_view
import kotlinx.android.synthetic.main.peliculas_item.view.imageView
import kotlinx.android.synthetic.main.peliculas_item.view.title_text_view

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    private val movieList = ArrayList<Movie>()

    private val differCallback= object : DiffUtil.ItemCallback<Movie>(){
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }

    }
    val differ= AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.peliculas_item,parent,false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = differ.currentList[position]
        holder.itemView.apply {
            title_text_view.text = movie.title
            description_text_view.text = movie.overview

            val posterUrl = "https://image.tmdb.org/t/p/w500/" + movie.posterPath
            Glide.with(imageView.context)
                .load(posterUrl).into(imageView)
        }
    }

    override fun getItemCount(): Int {
        return movieList.size
    }
}