package com.example.examenpeliculas.ui.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import com.example.examenpeliculas.R
import com.example.examenpeliculas.databinding.ActivityDashboardBinding
import com.example.examenpeliculas.repository.Repository
import com.example.examenpeliculas.ui.PeliculasViewModel
import com.example.examenpeliculas.ui.ViewModelProviderFactory
import com.example.examenpeliculas.ui.fragments.CargaImagenesFragment
import com.example.examenpeliculas.ui.fragments.PeliculasFragment
import com.example.examenpeliculas.ui.fragments.PerfilFragment
import com.example.examenpeliculas.ui.fragments.UbicacionesFragment
import com.example.examenpeliculas.utils.Constants
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.SettingsDialog

class DashboardActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks,
    CargaImagenesFragment.HandlerActivity{

    private lateinit var vBind: ActivityDashboardBinding
    private val vModel: PeliculasViewModel by viewModels {
        ViewModelProviderFactory(application, Repository())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vBind = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(vBind.root)
        vModel.getPopularMovieResponse()
        vModel.getMoviesTopRatedResponse()
        vModel.getMoviesUpcomingResponse()
        vModel.getPersonPopular()

        replaceFragment(PerfilFragment())
        vBind.bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.perfil -> replaceFragment(PerfilFragment())
                R.id.peliculas -> replaceFragment(PeliculasFragment())
                R.id.ubicaciones -> replaceFragment(UbicacionesFragment())
                R.id.upload -> replaceFragment(CargaImagenesFragment())
                else->{}
            }
            true
        }

        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){

        }.launch(arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ))
        requestAccessFineLocationPermission()
        requestCameraPermission()
        requestWriteStoragePermission()
        requestReadStoragePermission()
    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout,fragment)
        fragmentTransaction.commit()
    }

    private fun requestAccessFineLocationPermission(){
        EasyPermissions.requestPermissions(
            this,
            "Se necesita el permiso de localizacion",
            Constants.PERMISSION_REQUEST_CODE,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    private fun requestCameraPermission(){
        EasyPermissions.requestPermissions(
            this,
            "Se necesita el permiso de la camara para poder tomar la foto",
            Constants.PERMISSION_REQUEST_CODE,
            Manifest.permission.CAMERA
        )
    }

    private fun requestWriteStoragePermission(){
        EasyPermissions.requestPermissions(
            this,
            "Se necesita el permiso de almacenamiento",
            Constants.PERMISSION_REQUEST_CODE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }

    private fun requestReadStoragePermission(){
        EasyPermissions.requestPermissions(
            this,
            "Se necesita el permiso de almacenamiento",
            Constants.PERMISSION_REQUEST_CODE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            SettingsDialog.Builder(this).build().show()
        }
        else{
            requestAccessFineLocationPermission()
            requestCameraPermission()
            requestWriteStoragePermission()
            requestReadStoragePermission()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {

    }

    val pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()){ uri->
        if (uri!= null){
            Log.i("imagen","Seleccionada")
            vModel.uriImage.postValue(uri)
        }
        else{
            Log.i("imagen","No Seleccionada")
        }
    }
    override fun abrirGaleria() {
        pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }
}