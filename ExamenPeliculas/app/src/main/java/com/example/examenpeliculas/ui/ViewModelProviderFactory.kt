package com.example.examenpeliculas.ui

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.examenpeliculas.repository.Repository

class ViewModelProviderFactory (
    val app: Application,
    val repository: Repository
): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PeliculasViewModel(app, repository) as T
    }
}