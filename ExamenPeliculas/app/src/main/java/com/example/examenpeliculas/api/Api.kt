package com.example.examenpeliculas.api

import com.example.examenpeliculas.models.response.AuthenticationTokenResponse
import com.example.examenpeliculas.models.response.MovieResponse
import com.example.examenpeliculas.models.response.PersonPopularResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface Api {

    @GET("movie/popular")
    suspend fun getPopularMovie(@Header("Authorization") token: String,@Query("page") page: Int): Response<MovieResponse>

    @GET("movie/top_rated")
    suspend fun getMoviesTopRated(@Header("Authorization") token: String,@Query("page") page: Int): Response<MovieResponse>

    @GET("movie/upcoming")
    suspend fun getMoviesUpcoming(@Header("Authorization") token: String,@Query("page") page: Int): Response<MovieResponse>

    @GET("person/224513")
    suspend fun getPersonPopular(@Header("Authorization") token: String,@Query("page") page: Int): Response<PersonPopularResponse>

}