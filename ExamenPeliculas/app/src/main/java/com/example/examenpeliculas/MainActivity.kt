package com.example.examenpeliculas

import android.app.Application
import android.os.Bundle
import androidx.activity.ComponentActivity
import com.example.examenpeliculas.utils.Prefs

class MainActivity : Application(){
    companion object {
        lateinit var prefs: Prefs
    }
    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(applicationContext)
    }
}