package com.example.examenpeliculas.ui.fragments

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.examenpeliculas.R
import com.example.examenpeliculas.databinding.FragmentPeliculasBinding
import com.example.examenpeliculas.databinding.FragmentUbicacionesBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class UbicacionesFragment : Fragment() , OnMapReadyCallback,
    GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener{

    private var _binding: FragmentUbicacionesBinding? = null
    private val vBind get() = _binding!!
    private lateinit var map: GoogleMap
    var latDestino: Double = 0.0
    var lngDestino: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUbicacionesBinding.inflate(inflater, container, false)
        return vBind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createMapFragment()
    }

    private fun createMapFragment() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.fm_map_store) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        this.map = googleMap
        this.map.setOnMyLocationButtonClickListener(this)
        this.map.setOnMyLocationClickListener(this)
        this.map.isMyLocationEnabled = true
        createMarker(latDestino, lngDestino)

    }

    private fun createMarker(lat: Double, lng: Double) {
        val coordinates = LatLng(lat, lng)
        val marker = MarkerOptions().position(coordinates).title("Ubicacion")
        this.map.addMarker(marker)
        this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f), 4000, null)
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }

    override fun onMyLocationClick(p0: Location) {

    }
}