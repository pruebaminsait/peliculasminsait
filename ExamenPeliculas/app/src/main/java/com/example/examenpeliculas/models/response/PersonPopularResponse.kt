package com.example.examenpeliculas.models.response

data class PersonPopularResponse (
    val birthday: String?,
    val biography: String?,
    val name: String?,
    val place_of_birth: String?,
    val profile_path: String?
    )

