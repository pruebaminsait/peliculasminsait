package com.example.examenpeliculas.ui.fragments

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.examenpeliculas.adapters.MovieAdapter
import com.example.examenpeliculas.databinding.FragmentPeliculasBinding
import com.example.examenpeliculas.ui.PeliculasViewModel
import com.example.examenpeliculas.utils.Resource
import kotlinx.android.synthetic.main.fragment_peliculas.rvPopulares

class PeliculasFragment : Fragment() {

    private var _binding: FragmentPeliculasBinding? = null
    private val vBind get() = _binding!!
    private val vModel by activityViewModels<PeliculasViewModel>()
    private lateinit var movieAdapter: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPeliculasBinding.inflate(inflater, container, false)
        return vBind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerViewPopulares()
        //vModel.getPopularMovieResponse()
        vModel.popularMovies.observe(viewLifecycleOwner, Observer { response->
            when (response) {
                is Resource.Success -> {
                    response.data?.let { newResponse ->
                        movieAdapter.differ.submitList(newResponse.results)
                    }
                }
                is Resource.Error -> {
                    response.message?.let { message ->
                        Log.e(ContentValues.TAG, "An error occured: $message")
                        Toast.makeText(activity, "An error occurred: $message", Toast.LENGTH_LONG)
                            .show()
                    }
                }
                is Resource.Loading -> {
                }
            }

        })
    }

    private fun setupRecyclerViewPopulares() {
        movieAdapter = MovieAdapter()
        rvPopulares.apply {
            adapter = movieAdapter
            layoutManager = LinearLayoutManager(activity)
            addOnScrollListener(this@PeliculasFragment.scrollListener)
        }
    }

    val scrollListener = object : RecyclerView.OnScrollListener() {

    }
}