package com.example.examenpeliculas.ui.fragments

import android.content.ContentValues
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.examenpeliculas.R
import com.example.examenpeliculas.databinding.FragmentCargaImagenesBinding
import com.example.examenpeliculas.databinding.FragmentPerfilBinding
import com.example.examenpeliculas.ui.PeliculasViewModel
import com.example.examenpeliculas.ui.activity.DashboardActivity
import com.example.examenpeliculas.utils.Resource
import kotlinx.android.synthetic.main.peliculas_item.view.imageView

class PerfilFragment : Fragment() {

    private var _binding: FragmentPerfilBinding? = null
    private val vBind get() = _binding!!
    private val vModel by activityViewModels<PeliculasViewModel>()
    interface HandlerActivity {

    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        handler = context as? DashboardActivity
    }
    private var handler: CargaImagenesFragment.HandlerActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPerfilBinding.inflate(inflater, container, false)
        return vBind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vModel.personPopular.observe(viewLifecycleOwner, Observer { response->
            vBind.tvName.text = response.data?.name
            vBind.tvBirthday.text = response.data?.birthday
            vBind.tvNacimiento.text = response.data?.place_of_birth
            vBind.tvBiography.text = response.data?.biography

            val posterUrl = "https://image.tmdb.org/t/p/w500/" + response.data?.profile_path
            Glide.with(vBind.ivPhoto.context)
                .load(posterUrl).into(vBind.ivPhoto)
        })

    }
}