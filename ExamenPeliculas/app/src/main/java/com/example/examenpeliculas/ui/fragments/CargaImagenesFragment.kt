package com.example.examenpeliculas.ui.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.examenpeliculas.R
import com.example.examenpeliculas.databinding.FragmentCargaImagenesBinding
import com.example.examenpeliculas.databinding.FragmentPeliculasBinding
import com.example.examenpeliculas.ui.PeliculasViewModel
import com.example.examenpeliculas.ui.activity.DashboardActivity

class CargaImagenesFragment : Fragment() {

    private var _binding: FragmentCargaImagenesBinding? = null
    private val vBind get() = _binding!!
    private val vModel by activityViewModels<PeliculasViewModel>()
    interface HandlerActivity {
        fun abrirGaleria()
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        handler = context as? DashboardActivity
    }

    private var handler: HandlerActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCargaImagenesBinding.inflate(inflater, container, false)
        return vBind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vBind.btnCargar.setOnClickListener {
            handler?.abrirGaleria()
        }
        vModel.uriImage.observe(viewLifecycleOwner, Observer { response ->
            vBind.ivPhotho.setImageURI(response)
        })

    }
}