package com.example.examenpeliculas.repository

import com.example.examenpeliculas.api.RetrofitInstance

class Repository {
    suspend fun getPopularMovies(token:String, page: Int) = RetrofitInstance.api.getPopularMovie(token,page)
    suspend fun getMoviesTopRated(token:String, page: Int) = RetrofitInstance.api.getMoviesTopRated(token,page)
    suspend fun getMoviesUpcoming(token:String, page: Int) = RetrofitInstance.api.getMoviesUpcoming(token,page)
    suspend fun getPersonPopular(token:String, page: Int) = RetrofitInstance.api.getPersonPopular(token,page)
}