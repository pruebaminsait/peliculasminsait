package com.example.examenpeliculas.ui

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.examenpeliculas.models.response.MovieResponse
import com.example.examenpeliculas.models.response.PersonPopularResponse
import com.example.examenpeliculas.repository.Repository
import com.example.examenpeliculas.utils.Constants
import com.example.examenpeliculas.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response

class PeliculasViewModel (
    app: Application,
    val repository: Repository
) : AndroidViewModel(app){

    var popularMovies : MutableLiveData<Resource<MovieResponse>> = MutableLiveData()
    var moviesTopRated : MutableLiveData<Resource<MovieResponse>> = MutableLiveData()
    var moviesUpcoming : MutableLiveData<Resource<MovieResponse>> = MutableLiveData()
    var personPopular : MutableLiveData<Resource<PersonPopularResponse>> = MutableLiveData()
    var uriImage: MutableLiveData<Uri> = MutableLiveData()

    fun getPopularMovieResponse()= viewModelScope.launch {
        popularMovies.postValue(Resource.Loading())
        val response = repository.getPopularMovies(Constants.BEARER_TOKEN,1)
        popularMovies.postValue(hadnlePopularMovie(response))
    }

    private fun hadnlePopularMovie(response: Response<MovieResponse>): Resource<MovieResponse>{
        if (response.isSuccessful){
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }

    fun getMoviesTopRatedResponse()= viewModelScope.launch {
        moviesTopRated.postValue(Resource.Loading())
        val response = repository.getMoviesTopRated(Constants.BEARER_TOKEN,1)
        moviesTopRated.postValue(hadnleMoviesTopRated(response))
    }

    private fun hadnleMoviesTopRated(response: Response<MovieResponse>): Resource<MovieResponse>{
        if (response.isSuccessful){
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }

    fun getMoviesUpcomingResponse()= viewModelScope.launch {
        moviesUpcoming.postValue(Resource.Loading())
        val response = repository.getMoviesUpcoming(Constants.BEARER_TOKEN,1)
        moviesUpcoming.postValue(hadnleMoviesUpcoming(response))
    }

    private fun hadnleMoviesUpcoming(response: Response<MovieResponse>): Resource<MovieResponse>{
        if (response.isSuccessful){
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }

    fun getPersonPopular()= viewModelScope.launch {
        personPopular.postValue(Resource.Loading())
        val response = repository.getPersonPopular(Constants.BEARER_TOKEN,1)
        personPopular.postValue(hadnlePersonPopular(response))
    }

    private fun hadnlePersonPopular(response: Response<PersonPopularResponse>): Resource<PersonPopularResponse>{
        if (response.isSuccessful){
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }
}