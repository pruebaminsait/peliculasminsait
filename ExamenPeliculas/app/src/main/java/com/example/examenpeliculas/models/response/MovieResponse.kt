package com.example.examenpeliculas.models.response

data class MovieResponse(
    val page: Int?,
    val totalResults: Int?,
    val totalPages: Int?,
    val results: List<Movie>?
)

data class Movie(
    val posterPath: String?,
    val id: Long,
    val title: String?,
    val overview: String?
)